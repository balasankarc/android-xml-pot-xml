#Balasankar C <c.balasankarc@gmail.com>
#Convert Android XML files to POT files for localisation
import os
from BeautifulSoup import BeautifulSoup
import re
os.system('mkdir pot_files')
for file in os.listdir('files'):
    inp = open('files/'+file)
    cwd = os.getcwd()    
    out = open('pot_files/'+file+".pot","w")
    soup = BeautifulSoup(inp)
    a = str(soup)
    b= re.findall(">.*<",a)
    for i in b:
        msgid= i[1:-1]
        msgid=msgid.replace(r'"',r'\"')  
        string1=r'msgid'+ "\"" + msgid + "\""
        string2=r'msgstr'+ "\"\""
        new_string1=string1.replace(r'\n',r'\\n')
        out.write("\n")
        out.write(new_string1)
        out.write("\n")
        out.write(string2)

