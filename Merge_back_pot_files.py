#Balasankar C <c.balasankarc@gmail.com>
#Convert POT Files generated from Android XML files to XML files
import os
from BeautifulSoup import BeautifulSoup
import re
if os.path.isdir('malayalam'):
    os.system("rm -r malayalam/")
os.system("mkdir malayalam")
for file in os.listdir('files'):
    inp = open('files/'+file)
    cwd = os.getcwd()    
    inp2 = open('pot_files/'+file+".pot")
    out = open('malayalam/'+file,"w")
    lines=inp2.readlines()
    lines2=lines
    count=0
    for cnt in lines2:
        count=count+1
    
    input_text=inp.readlines()
    m=1
    for i in input_text:
        if i=="\n":
            out.write(i)
        else:
            if m<count:
                pot_current=lines[m]
            if "<string" in i:
                xml_current = i.split(">")
                m=m+1
                try:
                    if m<count:
                        pot_current=lines[m]
                        if "msgstr\"\"" in pot_current:
                            pot_current=lines[m-1]                    
                except:
                    m=m-1
                regex=re.findall("\".*\"",pot_current)
                regex2=regex[0][1:-1]      
                xml_current=xml_current[0]+">" +regex2+"</string>"
                try:
                    m=m+1
                except:
                    m=m+0
                out.write(xml_current+"\n")
            else:
                out.write(i)
